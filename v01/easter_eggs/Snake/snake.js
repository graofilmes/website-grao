let snakePosition = [];
let food = [];
let running;
let maseLenght;
let snakeHead;
let snakeDirection;
let snakeSpeed;

var generateMase = function(){
  document.querySelector('section').style.gridTemplateRows = 'repeat('+ maseLenght +', 1fr)';
  document.querySelector('section').style.gridTemplateColumns = 'repeat('+ maseLenght +', 1fr)';

  let labirinto = '';
  let size = maseLenght * maseLenght;

  for(let i = 0; i < size; i++) {
    labirinto = labirinto + '<article></article>';
  }

  document.querySelector('section').innerHTML = labirinto;
}

var startPosition = function(){
  let rdm = Math.floor(Math.random()*maseLenght);

  snakePosition.push(rdm * maseLenght + rdm); //rdm * maseLenght = linhas... rdm = colunas
  snakeMove(snakePosition[0]);
}

var generateFood = function(){
  let foodPos = Math.floor(Math.random()*maseLenght*maseLenght);
  food.push(foodPos);
}

var turnSnake = function(key) {
  console.log('turn');
  if(key == 'ArrowUp' || key == 'ArrowDown' || key == 'ArrowLeft' || key == 'ArrowRight'){snakeDirection = key;}
  else if(key == ' ') {
    running = !running;
    if(running == true){moveSnake();}
  }
}

var moveSnake = function() {
  console.log('move');
  snakeHead = snakePosition[snakePosition.length-1];
  let newPosition;
  let upTurn = maseLenght*(maseLenght - 1);

  switch (snakeDirection) {
    case 'ArrowUp':
      if (Math.floor(snakeHead/maseLenght) == 0) {newPosition = snakeHead + upTurn;}
      else {newPosition = snakeHead - maseLenght;}
      break;
    case 'ArrowDown':
      if (Math.floor(snakeHead/maseLenght) == (maseLenght - 1)) {newPosition = snakeHead - upTurn;}
      else {newPosition = snakeHead + maseLenght;}
      break;
    case 'ArrowLeft':
      if (snakeHead%maseLenght == 0) {newPosition = snakeHead + (maseLenght - 1);}
      else {newPosition = snakeHead - 1;}
      break;
    case 'ArrowRight':
      if (snakeHead%maseLenght == (maseLenght - 1)) {newPosition = snakeHead - (maseLenght - 1);}
      else {newPosition = snakeHead + 1;}
      break;
    default:
    newPosition = snakeHead;
  }
  snakeMove(newPosition);
}

function snakeCrash() {
    let crash = false;
    for (i=0; i < snakePosition.length-1; i++) {
       if(snakePosition[i] == snakeHead) {
           crash = true;
           break;
       }
    }
    return crash;
}

var snakeMove = function(newSpot){
  console.log(snakePosition.length);
  let currentTarget = document.querySelectorAll("article");

  if (snakeCrash()){ //loose
    currentTarget[snakeHead].style.backgroundColor = "#003300";
    setTimeout( function(){document.querySelector("header").style.display = "block";}, 1500);

  } else if (snakePosition.length > 0.75 * maseLenght * maseLenght) { // Beat Level
      snakePosition = [];
      maseLenght += 5;
      generateMase();
      for(let i = 0; i <= Math.floor(maseLenght/10); i++){
        generateFood();
      }
      startPosition();

  } else { // Default Gameplay
    for( let fruit in food ) {
      if (food[fruit] == newSpot) {
        food.splice(fruit, 1);
        generateFood();
        if(snakeSpeed > 20) {snakeSpeed -= 5;}
        snakePosition.unshift(snakePosition[0]);
      }
      // comidas
      currentTarget[food[fruit]].style.backgroundColor = "red";
    }

    //move cobrinha
    currentTarget[snakePosition[0]].style.backgroundColor = currentTarget[snakePosition[0]].style.backgroundColor == "red" ? "red" : "#ccc";
    snakePosition.shift();
    snakePosition.push(newSpot);
    for( let position in snakePosition){
        currentTarget[snakePosition[position]].style.backgroundColor = "green";
    }

    if(running == true){ setTimeout(moveSnake, snakeSpeed) };
  }
}

// Start Game

var start = function() {
  console.log('start');
  maseLenght = parseInt(document.getElementsByName('maseSize')[0].value);
  document.querySelector("header").style.display = "none";

  snakePosition = [];
  food = [];
  running = true;
  maseLenght;
  snakeDirection = 'ArrowRight';
  snakeSpeed = 300;

  generateMase();
  for(let i = 0; i <= Math.floor(maseLenght/10); i++){
    generateFood();
  }
  startPosition();
}

// Event Lissener

window.onkeydown = function(e){
    turnSnake(e.key);
};
