Vue.component('default-section', {
  props: ['title'],
  template: `
    <section class="section">
      <article>
        <h1 v-if="title === 'Grão'" class="about-logo">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 337.4 175.6" style="enable-background:new 0 0 337.4 175.6;" xml:space="preserve">
            <path d="M201.3,103.5c0,1.9,1.4,23.1-19.7,23.1c-15.3,0-19.1-11.5-19.6-16.8h9.4c0.4,2,1.7,8.4,10.3,8.4c12.1,0,10.3-12.9,10.3-18.3
              c-5.3,6.6-18.3,8.5-26.4-0.3c-7.8-8.4-6.3-23.6-0.4-30.3c9.9-11,24.6-6.2,26.8-2.1v-3.4h9.4V103.5z M180.9,71.2
              c-15,0-14.9,26.1,0,26.1C195.9,97.3,196.3,71.2,180.9,71.2z"/>
            <path d="M213.1,63.9h9.4v2.4c1.7-1.8,9.4-6.2,17.2-1.1l-3.1,8.6c-9.8-5.1-14.1,0.5-14.1,9.7v21h-9.4V63.9z"/>
            <path d="M274.9,63.9v3.5c-6.8-8.8-31.8-6.6-31.8,16.7c0,22.2,22.6,25.9,31.8,17.4v3h9.4V63.9H274.9z M263.8,97.3
              c-14.9,0-15.2-26,0.1-26C279.4,71.2,279.5,97.3,263.8,97.3z"/>
            <path d="M315.8,62.8c28.6,0,29,42.8-0.2,42.8C287.2,105.6,286.9,62.8,315.8,62.8z M315.8,97.3c16.1,0,16.3-26,0-26
              C300,71.2,299.5,97.3,315.8,97.3z"/>
            <path d="M257.3,56.9l-5.5-4.6c3.4-4.9,5.7-7.7,10.6-7.7c5.3,0,10.7,4.4,13.1,4.4c1.6,0,3.3-1.5,5.2-4.5l5.3,4.4
              c-2.7,5.3-6.3,8-10.8,8C267.1,56.8,261.9,47.3,257.3,56.9z"/>
            <path d="M0,87.5c0,33.6,22.2,61.7,55.2,70.2c0.3-4.9,0.8-9.1,1.5-13c-26.9-8.1-42.3-28.9-42.3-57.2c0-20.7,7.4-36.5,25.9-50.9
              c7.5-5.8,14.5-11.5,15.8-20.8C23.1,24,0,53.4,0,87.5z"/>
            <path d="M81.2,0c-7.7,0.9-13,7.2-13.9,16.7c-1.5,15.5-12.4,23.7-15.2,25.6C34.7,54.1,25.2,67.3,25.2,87.1
              c0,21.8,14.5,41.3,35.3,47.4c2.2-5.6,4.8-9.1,6.6-11c0-0.1,0.1-0.1,0.1-0.2C41.7,116.8,39.7,94,39.7,87.1c0-8.7,5.3-19.9,17.1-28.5
              C74.4,45.8,81.2,37.5,81.2,0z"/>
            <path d="M147.8,87c0-33.3-24-63.8-55.8-71.1c-0.3,5-0.8,9.4-1.6,13.4c26.4,10,43.6,34.5,43.6,57.7c0,19-7.2,33.3-23.9,45.8
              c-11.9,8.9-15.9,14.9-16.8,24.7C112.4,154.7,147.8,128.5,147.8,87z"/>
            <path d="M104.4,124c12.7-9.8,19.1-21.2,19.1-37.1c0-19.7-15.9-40.5-36.3-47.6c-3,7.6-7.7,12.4-7.7,12.5C97,57.9,107.8,71.3,107.8,87
              c0,7.6-3.4,19.6-16.2,28.8c-22.4,16.1-25.6,22-25.6,59.6c5.3,0.5,9.9,0.5,15.2,0.1C81.2,143.3,85.4,138.8,104.4,124z"/>
          </svg>
        </h1>
        <h1 v-else-if="title" class="wbg">{{ title }}</h1>
        <slot></slot>
      </article>
    </section>
  `
})

Vue.component('default-slide', {
  props: ['title', 'bg'],
  template: `
    <article class="slide">
        <div class="slide-content">
          <img class="slide-img" :src="bg" />
          <h2 class="wbg">{{ title }}</h2>
          <span class="wbg"><slot></slot></span>
        </div>
    </article>
  `
})

var app = new Vue ({
  el: '#main',
  data: function(){
    return {
      options: {
        afterLoad: this.afterLoad,
        navigation: true,
        navigationPosition: 'left',
        anchors: ['Home', 'Sobre', 'Servicos', 'Trabalhos', 'Parceiros', 'Contato'],
        navigationTooltips: ['Home', 'Sobre', 'O que fazemos', 'Nossos trabalhos', 'Nossos parceiros', 'Contato'],
      },
      descricao: `<p>Com suas raízes na película cinematográfica, a <b>Grão</b> nasce com forte acento de brasilidade, referenciando também ícones de nossa cultura, como praias e cafezais.</p>
                  <p>Atualmente, a <b>Grão</b> produz conteúdos culturais críticos e inovadores, desenvolvendo tecnologias e almejando o impacto social.</p>
                  <p>Nossa especialidade é construir pontes entre novas possibilidades formais propiciadas pelas mídias digitais e as necessidades de conteúdos de nossos parceiros.</p>`,
      works: [
        {name: 'Viva a História', bg: 'bg/viva.jpg', highlight: false, type: 'media', path: 'works/viva.html', link: 'http://vivaahistoria.art.br'},
        {name: 'Tem em São Caetano?', bg: 'bg/scs.jpg', highlight: false, type: 'media', path: 'works/scs.html', link: 'http://tememsaocaetano.com.br'},
        {name: 'Mobiliário Urbano', bg: 'bg/moburb.jpg', highlight: false, type: 'media', path: 'works/moburb.html', link: 'http://moburb.org'},
        {name: 'Mostra Cinema e Direitos Humanos', bg: 'bg/mcdh.jpg', highlight: false, type: 'producao', path: 'works/mcdh.html'},
        {name: 'Engrenagens', bg: 'bg/engrenagens.jpg', highlight: false, type: 'media', path: 'works/engrenagens.html'},
        {name: 'Finalização de Audio e Vídeo', bg: 'bg/rancho.jpg', highlight: false, type: 'cinema', path: 'works/montagem.html'},
        {name: 'Revista Agreste', bg: 'bg/agreste.jpg', highlight: false, type: 'producao', path: 'works/agreste.html', link: 'http://revistaagreste.com.br'},
        {name: 'Produção Cinematográfica', bg: 'bg/sem_pena.jpg ', highlight: false, type: 'cinema', path: 'works/producao.html'},
        {name: 'Oficinas', bg: 'bg/oficinas.jpg', highlight: false, type: 'producao', path: 'works/oficinas.html'},
        {name: 'Caixinha de Música', bg: 'bg/caixinha.jpg', highlight: false, type: 'cinema', path: 'works/caixinha.html'},
        {name: 'Instalações Maria Thereza Alves', bg: 'bg/descolonizando.jpg', highlight: false, type: 'media', path: 'works/instalacoes.html'},
        {name: 'Mecânica Digital', bg: 'bg/mecanica.jpg', highlight: false, type: 'cinema', path: 'works/mecanica.html'},
        {name: 'Noite do Beijo – Ontem e Hoje', bg: 'bg/beijo.jpg', highlight: false, type: 'cinema', path: 'works/beijo.html'},
        {name: 'Iminência', bg: 'bg/iminencia.jpg', highlight: false, type: 'cinema', path: 'works/iminencia.html'},
        {name: 'Máquina de Histórias', bg: 'bg/maquina.jpg', highlight: false, type: 'media', path: 'works/maquina.html'},
        {name: 'Residência Pontos MIS', bg: 'bg/MIS.jpg', highlight: false, type: 'producao', path: 'works/mis.html'},
        {name: 'A Vida com Efeito', bg: 'bg/vida.jpg', highlight: false, type: 'cinema', path: 'works/vida.html'},
        {name: 'O Preço do Pão', bg: 'bg/pao.jpg', highlight: false, type: 'media', path: 'works/pao.html'},
      ],
      categories: {
        cinema:   {key: 'cinema',   active: true, triggered: false, highlight: false, showCat: false, bound: false, name: 'Cinema',            img: 'bg/lourenco.jpg',
                  description: 'Uma arte com mais de um século, o Cinema conquistou seu lugar ao sol ao combinar imagens e sons em narrativas dinâmicas e objetivas. Nossos projetos cinematográficos contam histórias que buscam maximizar a conexão com o público, explorando formatos consolidados como o documentário, o vídeoclipe, etc.'},
        media:    {key: 'media',    active: true, triggered: false, highlight: false, showCat: false, bound: false, name: 'Novas Narrativas',  img: 'bg/moburb.jpg',
                  description: 'A revolução digital trouxe ainda mais velocidade e conectividade ao mundo, nossos projetos em novas narrativas buscam investigar formas surpreendentes de contar histórias para públicos personalizados, investigando as infinitas possibilidades de um clique para criar momentos de magia em forma de audiovisual.'},
        producao: {key: 'producao', active: true, triggered: false, highlight: false, showCat: false, bound: false, name: 'Produção Cultural', img: 'bg/mcdh.jpg',
                  description: 'Além de contar histórias e desenvolver tecnologias, buscamos produzir impacto social, seja realizando oficinas, produzindo mostras ou criando materiais de difusão artística e política, como é caso da revista AGRESTE. Acreditamos no valor pedagógico da arte e comunicação para melhorar o mundo.'},
      },
      mediaBox: { show: false, title: '', link: undefined },
      clientes: [
        {alt: "Sesc", img: "sesc-logo.png"},
        {alt: "Museu da Imagem e do Som", img: "mis-logo.png"},
        {alt: "Secretaria da cultura ", img: "CULTURA_H_Traco.jpg"},
        {alt: "Instituto Cultura em Movimento", img: "icem.jpg"},
        {alt: "Revista Agreste", img: "agreste-logo.png"},
        {alt: "A Próxima Companhia", img: "proxima-logo.png"},
      ]
    }},
  methods: {
    afterLoad(origin, destination, direction) {
      if(destination.index){
        if(document.querySelectorAll('.section')[destination.index].querySelector('h1')) {
          document.querySelectorAll('.section')[destination.index].querySelector('h1').classList.add("show-title");
        }
        if(document.querySelectorAll('.section')[destination.index].querySelector('.about')) {
          document.querySelectorAll('.section')[destination.index].querySelector('.about').classList.add("show-letters");
        }
      }
      //back to original state
      if(origin && origin.index){
        if(document.querySelectorAll('.section')[origin.index].querySelector('h1')) {
          document.querySelectorAll('.section')[origin.index].querySelector('h1').classList.remove("show-title");
        }
        if(document.querySelectorAll('.section')[origin.index].querySelector('.about')) {
          document.querySelectorAll('.section')[origin.index].querySelector('.about').classList.remove("show-letters");
        }
      }
    },
    categoryTrigger(cat, hover) {
      let checkTrigger = !this.categories[cat].triggered;
      for (let category in this.categories) {
        if(checkTrigger || this.categories[cat].bound){
          this.categories[category].active = category === cat ? true : false;
          this.categories[category].triggered = category === cat ? true : false;
          this.categories[category].bound = category === cat ? this.categories[category].bound : false;
          this.categories[cat].showCat = hover ? true : false;
        } else {
          this.categories[category].active = true;
          this.categories[category].triggered = false;
          this.categories[cat].showCat = false;
        }
      }
    },
    highlightTrigger(element) {
      element.highlight = !element.highlight;
      this.categories[element.type].highlight = !this.categories[element.type].highlight;
    },
    summonMedia(work) {
      if(work){
        this.mediaBox.title = work.name;
        this.mediaBox.link = work.link ? work.link : undefined;
        var request = new XMLHttpRequest();
        request.open('GET', work.path, true);

        request.onload = function() {
          if (request.status >= 200 && request.status < 400) {
            // Success!
            document.querySelector(".loadedContent").innerHTML = request.responseText;
          } else {
            // We reached our target server, but it returned an error

          }
        };

        request.onerror = function() {
          // There was a connection error of some sort
        };

        request.send();

      } else {
        setTimeout(function(){document.querySelector(".loadedContent").innerHTML = ''; }, 1000);
      }
      this.mediaBox.show = !this.mediaBox.show;
    },
  }
})
