import Vue from 'vue'
import App from './App.vue'
import router from './router'
var VueTouch = require('vue-touch')
import vueVimeoPlayer from 'vue-vimeo-player'
import VueYoutube from 'vue-youtube'

Vue.use(VueTouch, {name: 'v-touch'})
Vue.use(vueVimeoPlayer)
Vue.use(VueYoutube)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
