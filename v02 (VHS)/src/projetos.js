const works = [
  {id: 1, name: 'Corpos, Territórios e Saberes',     img: 'ideias-e-acoes.jpg', type: 'midias',
         texto: `<p><b>Corpos, Territórios e Saberes: modos de agir e pensar nos ambientes</b> foi uma <b>instalação audiovisual</b> realizada para o SESC Sorocaba baseada em cinco iniciativas da Mostra Ideias e Ações para um Novo Tempo, com histórias, trajetórias, os saberes e a relação dos idealizadores das ações com os ambientes em qual vivem.</p>
         <ul>
          <li>Expografia: Luciana Valsechi e Fabio Morello.</li>
         </ul>`
       },
  {id: 2, name: 'Idas&Vindas',                       img: 'idas.png',           type: 'midias',    link: 'http://idasevindasdoc.com.br',
         texto: `<p>A <b>GRÃO</b> realizou o <b>desenvolvimento</b> e o <b>design</b> deste webdocumentário interativo que discute o tema das migrações contemporâneas no país.</p>
         <p>O projeto foi realizado pelas jornalistas AMANDA KRAINER, DEBORAH TRUNK e NATHÁLIA MATOS como trabalho de conclusão de curso em jornalismo pela ESPM.</p>`
       },
  {id: 3, name: 'Mostra Cinema e Direitos Humanos',  img: 'mcdh.jpg',           type: 'producao',
        texto: `<p>A <b>GRÃO</b> foi a produtora paulistano da 12ª Mostra Cinema e Direitos Humanos, promovida pelo Ministério dos Direitos Humanos nas 26 capitais e no Distrito Federal. Nessa edição, a Mostra teve como tema os 70 anos da Declaração Universal dos Direitos Humanos.</p>
        <p>Em São Paulo as sessões ocorreram nos cinemas da Galeria Olido e no Matilha Cultural, em diversos CEUs da cidade e em equipamentos municipais e Estaduais, como o programa Centro Aberto, os CICs e as Fundações Casa.</p>`
      },
  {id: 4, name: 'Noite do Beijo – Ontem e Hoje',     img: 'beijo.jpg',          type: 'cinema',
         texto: `<p>O documentário <b>Noite do Beijo - Ontem e Hoje</b>, realizado pela <b>GRÃO</b> propõe um diálogo entre gerações de jovens que participaram de momentos políticos marcantes da história de Sorocaba (SP). </p>
         <p>O filme é inspirado pelo mítico episódio Noite do Beijo - ocorrido em 1981 - e costura este evento a uma série de narrativas protagonizadas pela juventude nas décadas seguintes, culminando nas ocupações escolares de 2015. Suor, lutas e muitos hormônios misturam-se em uma trama que mistura ficção e realidade.</p>`
       },
  {id: 5, name: 'Tem em São Caetano?',               img: 'scs.jpg',            type: 'midias',    link: 'http://tememsaocaetano.com.br/', video: 'zVvx4w2I-tc',
         texto: `<p><b>TEM EM SÃO CAENTANO</b> é um webdocumentário construído em parceria com os alunos da EJA da EME Prof. Vicente Bastos.</p>
         <p>Seu formato busca simular o próprio processo de entrevistas, num diálogo entre alunos e entrevistados.</p>
         <p>A <b>GRÃO</b> ministrou a <b>Oficna de Webdoc</b>, realizou a <b>programação</b> do webdoc e a <b>montagem</b> do documentário <i>Aquém da Fundação: outras matizes em São Caetano</i>/</p>`
       },
  {id: 6, name: 'Caixinha de Música',                img: 'caixinha.jpg',       type: 'cinema',    link: 'https://www.youtube.com/playlist?list=PL9da3gfWt9cmfNzsn4nwm5zafKsrdZPiU',  video: 'SvvAXLUFUEY',
          texto: `<p><b>CAIXINHA DE MÚSICA</b> foi um espaço aberto no Sesc Sorocaba durante os dias do festival FEBRE em 2017.</p>
          <p>Alí, diversos artistas puderam entrar e gravar uma versão acústica de uma música de sua autoria, com qualidade de som e imagem.</p>
          <p>A <b>GRÃO</b> realizou a gravação de som e imagem da ação, cujos vídeos podem ser acessados pelo <a href="https://www.youtube.com/playlist?list=PL9da3gfWt9cmfNzsn4nwm5zafKsrdZPiU" target="_blank">canal de youtube do festival</a>.</p>`
        },
  {id: 7, name: 'Viva a História',                   img: 'viva.jpg',           type: 'midias',    link: 'http://vivaahistoria.art.br',
        texto: `<p>A <b>GRÃO</b> ministrou a <b>Oficna de Webdoc</b> e realizou a <b>programação</b> do webdocumentário <b>VIVA A HISTÓRIA</b>, que é uma colagem de fragmentos das diversas atividades que ocorreram na Escola Estadual de Ensino Integral Prof. Afrânio Lages, em Maceió, no ano de 2017. Memória de momentos reflexivos, transformadores e festivos do encontro entre educadores, artistas e de jovens guerreiros alagoanos, em busca de uma escola pública, gratuita e de qualidade e de um mundo melhor pra se viver.</p>`
      },
  {id: 8, name: 'Revista Agreste',                   img: 'agreste.jpg',        type: 'producao',  link: 'http://revistaagreste.com.br',
         texto: `<p><b>AGRESTE</b> é uma revista virtual de arte e política coltada às narrativas experimentais das mais diversas manifestações culturais.</p>
         <p>Além de textos teóricos, a revista conta com artigos experimentais em outras mídias, como foto, vídeo, narrativas interativas e publicações digitais.</p>
         <p>A <b>GRÃO</b> desenvolve o trabalho gráfico e editorial da revista com sazonalidade anual.</p>
         <p>A revista pode ser acessda pelo link <a href="http://revistaagreste.com.br/" target="_blank">revistaagreste.com.br</a>.</p>`
       },
  {id: 9, name: 'Mobiliário Urbano',                 img: 'moburb.jpg',         type: 'midias',    link: 'http://moburb.org',
         texto: `<p><b>Mobiliário Urbano</b> é um webdocumentário pesquisa os processos históricos e atuais de expulsão de populações tradicionais do centro da cidade de São Paulo.</p>
         <p>Realizado pela <b>GRÃO</b> com financiamento do edital <b>ProAc</b>, a plataforma desenvolve uma narrativa de montagem aleatória, que permine tma nova experiêcia a cada acesso.</p>
         <p>O Webdoc pode ser acessdo pelo link <a href="http://moburb.org" target="_blank">moburb.org</a>.</p>`
       },
  {id:10, name: 'Engrenagens',                       img: 'engrenagens.jpg',    type: 'midias',    video: 200321656,
         texto: `<p>Vídeo instalação realizada em 2016, na cidade de Sorocaba, como parte do evento <i>Experimentassom</i> no <b>SESC Sorocaba</b>, em parceria com o gurpo Cunhantã de teatro.</p>
         <p>A projeção com 12 metros de comprimento criu um ambiente imersivo para os expectadores e servia de palco a uma performance criada a partir de estudos da obra "Parque Industrial" (1932), de Patrícia Galvão, a Pagu.</p>`
       },
  {id:11, name: 'Finalização de Audio e Vídeo',      img: 'rancho.jpg',         type: 'cinema',    video: 262039149,
         texto: `<p>A <b>GRÃO</b> realiza o trabalho de montagem cinematográfica, edição e mixagem de som em diversos fimes. Dentre eles destacam-se:</p>
         <ul>
           <li>Um Filme de Cinema (Longa-metragem) – 2017, Thiago B. Mendonça, 108 min, cor</li>
           <li>Um Filme de Cinema (Série) –  2017, Thiago B. Mendonça, 13 episódios, 13 min, cor</li>
           <li>A Pajé – 2019, Felipe Ludovice, 25 min, cor</li>
           <li>Rancho da Goiabada – 2019, Guilherme Martins, 20 min, cor</li>
         </ul>`
       },
  {id:12, name: 'Oficinas',                          img: 'oficinas.jpg',       type: 'producao',
        texto: `<p>A <b>GRÃO</b> realiza <b>oficinas</b> de formação em <b>linguagem cinematográfica</b> e <b>novas narrativas</b>, realizando núcleos de estudo e produção cinematográfica e oficinas de webdocumentário e programação web.</p>`
      },
  {id:13, name: 'Produção Cinematográfica',          img: 'sem_pena.jpg',       type: 'cinema',    video: 'DsnZIMkk_9o',
         texto: `<p>A <b>GRÃO</b> realiza o trabalho de produção executiva e de set em diversos fimes. Dentre eles destacam-se:</p>
         <ul>
           <li>Sem Pena – 2014, Eugênio Puppo, 89 min, cor</li>
           <li>Coragem – 2016, Sebastião Braga, 72 min, cor</li>
           <li>Curtas  Jornadas Noite Adentro – 2019, Thiago B. Mendonça, em montagem, cor</li>
           <li>Cidade dos Abismos –  2019, Priscila Betim e , em montagem, cor</li>
         </ul>`
       },
  {id:14, name: 'Instalações Maria Thereza Alves',   img: 'descolonizando.jpg', type: 'midias',    video: 'fePUi5qiUhA',
         texto: `<p>A <b>GRÃO</b> realizou a edição de vídeo e assessoria técnica de alguns dos projetos mais recentes da artista Maria Thereza Alves, reconhecida mundialmente, colaborando na construção de instalações baseadas na exibição simultânea de vídeos em múltiplas telas:
         <ul>
           <li>Um Vazio Pleno (A Full Void) - <i>Frestas</i>, Sorocaba, 2017</li>
           <li>To See the Forest Standing - <i>Disappearing Legacies: The World as Forest</i>, Hamburgo, 2017</li>
           <li>Descolonizando o Brasil (Decolonizing Brazil), <a href="http://www.descolonizandobrasil.com.br" target="_blank">Website Interativo<sup>*</sup></a> e exposição no SESC, Sorocaba, 2019.</li>
         </ul>
         <span style="font-size: x-small">
           <sup>*</sup>Esse website interativo não foi desenvolvido pela <b>GRÃO</b>.
         </span>`
       },
  {id:15, name: 'A Vida com Efeito',                 img: 'vida.jpg',           type: 'cinema',    video: 44046105,
        texto: `<p>Buscando se desprender de formalidades tecnicas, Lourenço Mutarelli se põe quase todas as noites sob o efeito de álcool, café e antidepressivos. Este experimento resulta em diversos cadernos de rascunho que refletem os principais temas de sua vida, como a infância conturbada, problemas psicológicos, suas idéias e as dificuldades cotidianas que o autor enfrenta.</p>
        <p>O documentário <b>A VIDA COM EFEITO</b> que acompanha o artista em sua vida pessoal e seu processo criativo foi realizado pela <b>GRÃO</b> em 2006.</p>`
      },
  {id:16, name: 'Máquina de Histórias',              img: 'maquina.jpg',        type: 'midias',
         texto: `<p><b>MÁQUINA DE HISTÓRIAS</b> foi uma instalação interativa desenvolvida pela <b>GRÃO</b> realiza na <b>VIRADA CULTURAL 2013</b>.</p>
         <p>A instalação consistia em um totem dotado de um fone e um microfone, onde cada participante ouvia um trecho de história e era convidado a continuar a narrativa.</p>
         <p>A soma da interação dos mais de 370 usuários gerou ao todo 27 histórias, que depois de completas podiam ser escutadas pelos participantes do evento.</p>`
       },
  {id:17, name: 'Mecânica Digital',                  img: 'mecanica.jpg',       type: 'midias',    video: 70778401,
        texto: `<p><b>MECÂNICA DIGITAL</b> foi uma instalaçnao interativa realizada por Radamés Ajna que consistia num Gameboy 1989 com o jogo Tetris, explodido mas totalmente jogável através de um interface arcade com um jostick e botões.</p>
        <p>A <b>GRÃO</b> realizou o vídeo making-of exposto junto com a obra na Exposição Mais de Mil Brinquedos para a Criança Brasileira no SESC Pompéia, ocorrida de 9.jul.2013 a 2.fev.2014.</p>`
      },
  {id:18, name: 'Iminência',                         img: 'iminencia.jpg',      type: 'cinema',    link:'http://iminencia.com.bt',
              texto: `<p>A <b>GRÃO</b> foi a produtora executiva das produções do projeto de circulação <b>IMERSÃO IMINÊNCIA</b>, do coletivo <b>IMINÊNCIA</b>, fomentado pelo <b>ProAc</b>.</p>
              <p>O projeto consistiu numa ponte entre o coletivo paulistano e outros quatro coletivos inependentes do cenário paulista, que resultou em encontros, oficinas cruzadas e produções conjuntas.</p>
              <p>Os vídeos do processo podem ser acessados pelo <a href="http://iminencia.com.br" target="_blank">website do coletivo</a>.</p>`
            },
  {id:19, name: 'O Preço do Pão',                    img: 'pao.jpg',            type: 'midias',
         texto: `<p><b>O PREÇO DO PÃO</b> foi um espetáculo teatral transmídia livremente inspirado no livro <i>Inútil Canto e Inútil Pranto pelos Anjos Caídos</i> de Plínio Marcos.</p>
         <p>Cruzando as linguagens linguagens teatral e audiovisual, circulou por diversas cidades da grande São Paulo e realizou temporada na capital, com o apoio do <i>ProAc Primeiras Obras de Teatro</i>.</p>
         <p>A <b>GRÃO</b> desenvolveu as projeções mapeadas que constituiram o cenário e parte da iluminação do espetáculo.</p>`
       },
  {id:12, name: 'Residência Pontos MIS',             img: 'MIS.jpg',            type: 'producao',  video: 96805944,
        texto: `<p>A <b>GRÃO</b> realizou a <b>montagem</b> dos filmes do programa de residência Pontos MIS 2013, com a montagem de nove curtas-metragem de cineastas iniciantes selecionados pelo programa.</p>
        <p>Sob supervisão da montadora Cristina Amaral, a equipe da <b>GRÃO</b> também realizou suporte educativo do curso de montagem Aprender fazendo, fazer pensando no MIS-SP.</p>`
      },
]

export default works
